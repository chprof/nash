module.exports = function() {
	$.gulp.task("images", async function(done) {
		return $.gulp.src(['./app/images/**/*.{jpg,jpeg,png,gif,svg}'])
			.pipe($.gp.newer('./build/images/'))
			.pipe($.gp.if($.minificateImg, $.gp.imagemin([
				$.jpegRecompress({
					loops: 1,
					quality: "low",
					min: 60,
					max: 70
				}),
				$.imageminSvgo({
					plugins: [
						{
							removeUselessStrokeAndFill: false
						}
					]
				}),
				$.pngQuant({quality: [0.8, 0.9], speed: 1})
			])))
			.pipe($.gulp.dest("./build/images/"))
			.on('end', done);
	});
	$.gulp.task('video', async function(){
		return $.gulp.src('./app/video/*')
			.pipe($.gulp.dest('./build/video/'))
	})
};