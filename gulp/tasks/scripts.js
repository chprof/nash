module.exports = function() {
	$.gulp.task("scripts", async function() {
		return $.gulp.src("./app/js/main.js")
			.pipe($.gulp.dest("./build/js/"))
			.on("end", $.bs.reload);
	});
	$.gulp.task("bundle", async function() {
		return $.gulp.src([
				"node_modules/owl.carousel/dist/owl.carousel.min.js",
				// "node_modules/magnific-popup/dist/jquery.magnific-popup.min.js",
				"node_modules/svg4everybody/dist/svg4everybody.min.js",
				"node_modules/popper.js/dist/umd/popper.min.js",
				"node_modules/bootstrap/dist/js/bootstrap.min.js",
				"node_modules/inputmask/dist/min/jquery.inputmask.bundle.min.js",
				"node_modules/wow.js/dist/wow.min.js"
			])
			.pipe($.gp.concat('vendor.min.js'))
			.pipe($.gulp.dest("./build/js/"))
			.on("end", $.bs.reload);
	});
	$.gulp.task('critical', async function () {
		return $.gulp.src('./build/*.html')
			.pipe($.critical({
				base: './build/',
				inline: true,
				css: ['./build/css/critical.css', './build/css/vendorCss.css', './build/css/style.css']})
			).on('error', function(err) {
				$.log.error(err.message); 
			})
			.pipe($.gulp.dest('./build/'));
	});
};