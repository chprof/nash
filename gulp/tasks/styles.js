module.exports = function () {
	var processors = [
		$.prefix({
			browsers: ['last 10 version'],
			cascade: false
		}),
		$.mqpacker({
			sort: $.sortMedia
		})
	];
	function stylesFunc(path) {
		// return $.gulp.src("./app/scss/critical.scss")
		return $.gulp.src(path)
			.pipe($.gp.plumber({
				errorHandler: $.gp.notify.onError(function(err) {
					return {
						title: 'Plumber Error: Styles',
						message: err.message
					}
				})
			}))
			.pipe($.gp.sourcemaps.init())
			.pipe($.gp.sass({
				outputStyle: 'expanded',
				errLogToConsole: true,
				includePaths: 'node_modules'
			}).on('error', $.gp.sass.logError))
			.pipe($.gp.postcss(processors))
			.pipe($.gp.plumber.stop())
			.pipe($.gp.sourcemaps.write("./maps/"))
			.pipe($.gp.if($.minificate, $.gp.cleanCss({
				rebase: false
			}) ))
			.pipe($.gulp.dest("./build/css/"))
			.on("end", $.bs.reload);
	};
	$.gulp.task("criticalCss", async function() {
		return stylesFunc("./app/scss/critical.scss");
	})
	$.gulp.task("vendor", async function() {
		return stylesFunc("./app/scss/vendorCss.scss");
	})
	$.gulp.task("styles", async function () {
		return $.gulp.src("./app/scss/style.scss")
			.pipe($.gp.plumber({
				errorHandler: $.gp.notify.onError(function(err) {
					return {
						title: 'Plumber Error: Styles',
						message: err.message
					}
				})
			}))
			.pipe($.gp.sourcemaps.init())
			.pipe($.gp.sass({
				outputStyle: 'expanded',
				errLogToConsole: true
			}).on('error', $.gp.sass.logError))
			.pipe($.gp.postcss(processors))
			.pipe($.gp.plumber.stop())
			.pipe($.gp.sourcemaps.write("./maps/"))
			.pipe($.gp.if($.minificate, $.gp.cleanCss({
				rebase: false
			}) ))
			.pipe($.gulp.dest("./build/css/"))
			.on("end", $.bs.reload);
	});
};