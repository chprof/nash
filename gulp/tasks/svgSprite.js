module.exports = function() {
	$.gulp.task("svgSprite", async function() {
		return $.gulp.src('./app/icons/svg/*')
			.pipe($.gp.svgmin({
				js2svg: {
					pretty: true
				}
			}))
			.pipe($.gp.cheerio({
				run: function ($) {
					$('[fill]').removeAttr('fill');
					$('[stroke]').removeAttr('stroke');
					$('[style]').removeAttr('style');
				},
				parserOptions: {xmlMode: true}
			}))
			.pipe($.gp.replace('&gt;', '>'))
			.pipe($.gp.svgSprite({
				mode: {
					symbol: {
						dest: 'build/images/svg-sprite',
						sprite: 'svgSprite.svg',
						example: false,
						render: {
							css: false,
							scss: false
						}
					}
				}
			}))
			.pipe($.gulp.dest('./'))
	});
};