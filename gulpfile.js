global.$ = {
	gulp: require('gulp'),
	gp: require('gulp-load-plugins')(),
	bs: require('browser-sync').create(),

	prefix: require('autoprefixer'),
	mqpacker: require('css-mqpacker'),
	sortMedia: require('sort-css-media-queries'),

	pngQuant: require('imagemin-pngquant'),
	jpegRecompress: require('imagemin-jpeg-recompress'),
	imageminSvgo: require('imagemin-svgo'),
	del: require('del'),
	log: require('fancy-log'),
	critical: require('critical').stream,

	npmfiles: require('npmfiles'),
	
	path: {
		tasks: require('./gulp/config.js')
	},
	minificate: true,
	minificateImg: true,
	TunnelFtp: false
};


$.path.tasks.forEach(function(taskPath) {
	require(taskPath)();
});

$.gulp.task('pluginsList', function() {
	return console.log($.gp);
});
$.gulp.task('default', $.gulp.series('clean', 'html', $.gulp.parallel('favicons', 'fonts', 'criticalCss', 'vendor', 'styles', 'scripts', 'bundle', 'images', 'video', 'svgSprite'), 'critical', 'watch', 'serve'), function(done){
	done();
});


