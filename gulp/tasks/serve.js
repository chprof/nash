module.exports = function() {
	$.gulp.task("serve", function() {
		return new Promise((res,rej) => {
			$.bs.init({
				server: "./build/",
				tunnel: $.TunnelFtp,
				notify: false
			});
			res();
		});
	});
};